

import React, {Component} from 'react';
import {Platform,Dimensions, StyleSheet, Text, View,TextInput,TouchableOpacity,Image} from 'react-native';
import MapView ,{Polyline, Marker} from "react-native-maps";
import Modal from "react-native-modalbox";
import _ from "lodash";
import Button from 'react-native-button';
import PolyLine from"@mapbox/polyline"
import socketIO from "socket.io-client";
import RBSheet from "react-native-raw-bottom-sheet";

var {height,width}=Dimensions.get('window')

export default class Passenger extends Component{
    constructor(props){
        super(props)
        this.state={

            error:"",
            latitude:0,
            longitude:0,
            destination:'',
            predictions:[],
            pointCoords:[],
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            isDriver: false,
            isPassenger: false,

        }
        this.onChangeDestinationDebounced = _.debounce(
            this.onChangeDestination,
            1000
        )
    }
    onClose() {
        console.log('Modal just closed');
    }

    onOpen() {
        console.log('Modal just opened');
    }

    onClosingState(state) {
        console.log('the open/close of the swipeToClose just changed');
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                     latitude:position.coords.latitude,
                    longitude:position.coords.longitude



                })
                // this.getRouteDirections();
            },
            error => this.setState({error:error.message}),
            {enableHighAccuracy:true,maximumAge:2000, timeout:20000}
        )
    }

    async getRouteDirections(placeId,place_id){
        try{
            const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
            const  json = await response.json()
            // console.log(json)
            const points=PolyLine.decode(json.routes[0].overview_polyline.points)
            const pointCoords = points.map(point =>
                {
                    return{latitude:point[0],longitude:point[1]}
                }
            )
            this.setState({pointCoords, predictions:[],destination:place_id,routeResponse: json})
            this.refs.modal1.close()

            this.map.fitToCoordinates(pointCoords,{
                edgePadding:{top:30,bottom:30,left:30,right:30}

            })
        }
        catch (error) {
            console.error(error)

        }

    }


    async onChangeDestination (destination){
        const apikey="AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
        const apiUrl=`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}&input=${destination}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`

        try{
            const result = await fetch(apiUrl)
            const json = await result.json()
            console.log(json)
            this.setState({predictions:json.predictions})
        }catch(e){
            console.log(e)
        }
    }

    async requestDriver(){
        const socket = socketIO.connect("http://10.10.103.113:3000");

        socket.on("connect",()=>{
            console.log("client connected");
            //Request Taxi
            socket.emit("taxi Request",this.state.routeResponse)
        })

    }


    render() {
        let marker = null
        let driverButton = null ;
        let orderSheet = null;
        if(this.state.pointCoords.length >1) {
            marker = (
                <Marker coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]}/>
            );


            driverButton = (<View style={{ width: '90%',
                    height: "20%",
                    marginLeft:20,
                    marginBottom:20,
                    alignItems:"center",
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    position: 'absolute',
                    bottom: 0,
                    borderRadius:10}}>
                    <View>
                        <View style={{flexDirection:"row"}}>
                            <Image source={require('../props/BigPin.png')}
                                   style={{ height: 30,
                                       width: 30,marginLeft:10}}/>

                            <View style={{borderBottomWidth:1,color:"black",marginLeft:"3%",width:"80%"}}>
                                <Text style={{fontSize:16,marginBottom:20}} >Prasetiya Mulya University</Text>
                            </View>

                        </View>
                        <TouchableOpacity onPress={() => this.refs.modal1.open()} style={{flexDirection:"row",marginTop:10}}>
                            <Image source={require('../props/icons8-marker-96.png')}
                                   style={{ height: 30,
                                       width: 30,marginLeft:10}}/>

                            <Text

                                style={styles.btn}
                            >{this.state.destination}
                            </Text>
                        </TouchableOpacity>
                        <View style={{marginTop:10}}>
                            {driverButton}
                        </View>
                    </View>


                <TouchableOpacity
                    onPress={() => this.requestDriver()}
                    style={styles.bottomButton}>

                    <View>
                        <Text style={styles.bottomButtonText}>FIND DRIVER</Text>
                    </View>

                </TouchableOpacity>
                </View>

            );


        } else {
            orderSheet = (
                <View style={{ width: '90%',
                height: "20%",
                marginLeft:20,
                marginBottom:20,
                alignItems:"center",
                backgroundColor: 'white',
                justifyContent: 'center',
                position: 'absolute',
                bottom: 0,
                borderRadius:10}}>
                <View>
                    <View style={{flexDirection:"row"}}>
                        <Image source={require('../props/BigPin.png')}
                               style={{ height: 30,
                                   width: 30,marginLeft:10}}/>

                        <View style={{borderBottomWidth:1,color:"black",marginLeft:"3%",width:"80%"}}>
                            <Text style={{fontSize:16,marginBottom:20}} >Prasetiya Mulya University</Text>
                        </View>

                    </View>
                    <View style={{flexDirection:"row",marginTop:10}}>
                        <Image source={require('../props/icons8-marker-96.png')}
                               style={{ height: 30,
                                   width: 30,marginLeft:10}}/>
                        <Button onPress={() => this.refs.modal1.open()} style={styles.btn}>Im Going To</Button>
                    </View>
                    <View style={{marginTop:10}}>
                        {driverButton}
                    </View>
                </View>

            </View>)


        }



        const predictions = this.state.predictions.map(prediction =>(
            <TouchableOpacity
                key={prediction.id}
                onPress={()=>this.getRouteDirections(prediction.place_id,prediction.description)
                    }>
                <View style={{width:width,borderBottomWidth:1,marginBottom:5}}>
                    <Text style={styles.suggestion} key={prediction.id}>{prediction.description}</Text>
                </View>
            </TouchableOpacity>
        ))




        return (
            <View style={styles.container}>


                <MapView
                    ref={map =>
                        this.map = map
                    }

                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0200,
                    }}
                    showsUserLocation={true}
                >
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2}
                        strokeColor={"blue"}
                    />
                    {marker}
                </MapView>


                {driverButton}
                {orderSheet}
                <Modal
                    style={[styles.modal, styles.modal1]}
                    ref={"modal1"}
                    swipeToClose={this.state.swipeToClose}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    onClosingState={this.onClosingState}>
                    <View style={{flexDirection:"row"}}>
                        <Image source={require('../props/BigPin.png')}
                               style={{ height: 30,
                                   width: 30,marginLeft:10}}/>

                        <View style={{borderBottomWidth:1,color:"black",marginLeft:"3%",width:"80%"}}>
                            <Text style={{fontSize:16,marginBottom:20}} >Prasetiya Mulya University</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:"row",marginLeft:35,marginTop:10}}>
                    <Image source={require('../props/BigPin.png')}
                           style={{ height: 30,
                               width: 30,marginLeft:10}}/>
                    <TextInput
                        placeholder="Im Going To"
                        value={this.state.destination}
                        style={styles.destination}
                        onChangeText={destination=>{
                            this.setState({destination})
                            this.onChangeDestinationDebounced(destination)
                        }}
                        onSubmitEditing={() => {
                            this.refs.modal1.close();
                        }}
                    />
                    </View>

                    {predictions}

                    {/*<Button onPress={() => this.setState({swipeToClose: !this.state.swipeToClose})} style={styles.btn}>Disable swipeToClose({this.state.swipeToClose ? "true" : "false"})</Button>*/}

                </Modal>


                {/*{predictions}*/}
                {/*{driverButton}*/}
            </View>

        );
    }
}

const styles = StyleSheet.create({
    bottomButton:{
      backgroundColor:"green",
      marginTop: "auto",
      margin: 20,
      padding:15,
      alignSelf:"center",
      alignItems:"flex-end"

    },
    bottomButtonText:{
        color:"white",
        fontSize: 20

    },
    suggestion:{
        backgroundColor: "white",
        fontSize:18,
        marginBottom:10,
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        flex:1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    destination:{
        height:40,
        backgroundColor:"white",
        width:"100%",
        marginLeft:10


    },  modal: {
        paddingTop:50,
        paddingLeft:20,
        margin:0
    },
    btn: {
        marginTop: 10,
        backgroundColor: 'transparent',
        color: 'rgba(52, 52, 52, 0.8)',
        width:"100%",
        fontSize:16,
        marginLeft:"3%"
    },modal1: {

        alignItems: 'center',
        margin:0
    },
    modal4: {
        height: 300,
        margin:0
    },

});