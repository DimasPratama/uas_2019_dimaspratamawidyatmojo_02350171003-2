import React, {Component} from 'react';
import {Platform,Button, StyleSheet, Text, View,} from 'react-native';
import Passenger from "./Passenger";
import Driver from "./Driver";


export default class homeChoose extends Component{
    constructor(props){
        super(props);
        this.state = {
            isDriver:false,
            isPassenger:false
        }

    }


    render() {
        if(this.state.isDriver){
            return<Driver/>;
        }


        if(this.state.isPassenger){
            return<Passenger/>
        }

        return (
            <View style={styles.container}>
                <View style={{marginTop: 50}}>
                <Button
                    style={styles.button}
                    onPress={()=> this.setState({isPassenger: true})}
                    title="Passenger"
                />
                </View>

                <Button
                    style={styles.button}
                    onPress={()=> this.setState({isDriver: true})}
                    title="Driver"
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1
    },
    button: {
        marginTop: 25
    }
});